library(tidyverse)
library(patchwork)
library(ggspatial)

habitat_color <- c("MH01" = "#D74545", "MH02" = "#CF8160",
                   "MH03" = "#E5B86C", "MH04" = "#EACF8F",
                   "MH05" = "#E1E57E", "MH06" = "#AADE77",
                   "MH07" = "#A2C07C", "MH08" = "#7EDAAA",
                   "MH09" = "#85E8CA", "MH10" = "#82E7F2",
                   "MH11" = "#6DC6EE", "MH12" = "#6C93C5",
                   "MH13" = "#3D6FAE", "MH14" = "#2D4D71",
                   "MH15" = "#643D95", "MH16" = "#B738EB")

#download dataset at https://doi.org/10.15454/QSXKGA/NEWJ37
dat_geo <- read_csv("RMQS1_analyses_composites_18_11_2021_virgule.csv", na = "ND") |>
  filter(no_couche == 1) |>
  select(id_site,x_theo,y_theo) |>
  rename(x = x_theo, y = y_theo) |>
  mutate(id_site = as.character(id_site))

dat_mmb <- dataverse::get_dataframe_by_name(
    filename = "rmqs1_biomass.tab",
    dataset  = "10.57745/VLWJ51",
    server   = "entrepot.recherche.data.gouv.fr",
    original = TRUE,
    .f = \(.) readr::read_tsv(., col_types = "cd"))

dat_hab <- dataverse::get_dataframe_by_name(
    filename = "rmqs1_16S_microbial_habitat.tab",
    dataset  = "10.57745/JK5WCD",
    server   = "entrepot.recherche.data.gouv.fr",
    original = TRUE,
    .f = \(.) readr::read_tsv(., col_types = "cc"))

dat_phylum <- dataverse::get_dataframe_by_name(
    filename = "rmqs1_16S_taxonomy_phylum.tab",
    dataset  = "10.57745/WIRXIC",
    server   = "entrepot.recherche.data.gouv.fr",
    original = TRUE,
    .f = \(.) readr::read_tsv(., col_types = cols(id_site = "c",.default = "i")))

Sys.setenv("VROOM_CONNECTION_SIZE" = 1e9)
dat_ric <- dataverse::get_dataframe_by_name(
    filename = "rmqs1_16S_otu_abundance.tsv.gz",
    dataset  = "10.57745/ZZWKGQ",
    server   = "entrepot.recherche.data.gouv.fr",
    original = TRUE,
    .f = \(.) readr::read_tsv(., col_types = cols(id_site = "c",.default = "i"))) |>
  column_to_rownames("id_site") |>
  hillR::hill_taxa(q = 0) |>
  enframe("id_site","richness")

dat <- dat_geo |>
  left_join(dat_mmb) |>
  left_join(dat_hab) |>
  left_join(dat_phylum) |>
  left_join(dat_ric)

ggmap_args <- list(
  geom_tile(width = 1.6e4, height = 1.6e4, color = "white"),
  theme_void(),
  coord_sf(datum = sf::st_crs("EPSG:2154")),
  theme(text = element_text(size = 9)))

map_mmb <-
  ggplot(dat,aes(x,y, fill = biomass)) +
  labs(fill = "(a) Molecular microbial biomass\n     (log scale)") +
  ggmap_args +
  scale_fill_viridis_c(option = "D",na.value = "white", direction = -1,
                       trans = "log1p",breaks = c(1,10,100,600),labels = scales::label_number_auto())

map_ric <- ggplot(dat,aes(x,y, fill = richness)) +
  labs(fill = "(b) 16S richness") +
  ggmap_args +
  scale_fill_viridis_c(option = "F" ,na.value = "white", direction = -1,labels = scales::label_number_auto())

map_hab <- ggplot(dat,aes(x,y, fill = microbial_habitat)) +
  labs(fill = "(c) 16S Microbial habitat") +
  ggmap_args +
  scale_fill_manual(values = habitat_color,na.value = "white") +
  guides(fill = guide_legend(ncol = 2)) +
  annotation_scale(plot_unit = "m", location = "bl") +
  annotation_north_arrow(location = "bl",
                         pad_x  = grid::unit(0.95,"cm"),
                         pad_y  = grid::unit(0.75,"cm"),
                         height = grid::unit(1,"cm"),
                         width  = grid::unit(1,"cm"))

map_f <- ggplot(dat,aes(x,y, fill = Firmicutes)) +
  labs(fill = expression("(d)"~italic("Firmicutes")~"abundance")) +
  ggmap_args +
  scale_fill_viridis_c(option = "C",na.value = "white", direction = -1,labels = scales::label_percent(scale = 0.01))

map <- map_mmb + map_ric + map_hab + map_f  + plot_annotation(tag_levels = "a") + patchwork::plot_layout(ncol = 2,guides = "collect")
ggsave("un-maps.png",map , units = "cm", width = 26, height = 19.3, dpi = 300)

